// This Java API uses camelCase instead of the snake_case as documented in the API docs.
//     Otherwise the names of methods are consistent.

import hlt.*;

import java.util.*;

//import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

public class MyBot {
    public static void main(final String[] args) {
        final long rngSeed;
        if (args.length > 1) {
            rngSeed = Integer.parseInt(args[1]);
        } else {
            rngSeed = System.nanoTime();
        }
        //final Random rng = new Random(rngSeed);

        Game game = new Game();
        // At this point "game" variable is populated with initial map data.
        // This is a good place to do computationally expensive start-up pre-processing.
        // As soon as you call "ready" function below, the 2 second per turn timer will start.
        game.ready("AstralisJavaBot");

        Log.log("Successfully created bot! My Player ID is " + game.myId + ". Bot rng seed is " + rngSeed + ".");
				
        // To know what ships are on delivery
		ArrayList<EntityId> way_home = new ArrayList<EntityId>();

        for (;;) {
            game.updateFrame();
            final Player me = game.me;
            final GameMap gameMap = game.gameMap;

            final ArrayList<Command> commandQueue = new ArrayList<>();

            for (final Ship ship : me.ships.values()) {
            	
            	// 1. First ship is a Joker
            	if (ship.id.id == 1) {
            		if (ship.position.equals(game.players.get(1).shipyard.position)) {
            			commandQueue.add(ship.stayStill());
            		}
            		else {
            			commandQueue.add(ship.move(gameMap.naiveNavigate(ship, 
            					game.players.get(1).shipyard.position)));
            		}
            	}
            	
            	// 2. Block the enemy base, if we randomly get on it
            	else if (ship.position.equals(game.players.get(1).shipyard.position)) {
            		commandQueue.add(ship.stayStill());
            	}
            	
            	// 3. End game case - go home now quickly
            	else if (game.turnNumber >= Constants.MAX_TURNS - 20) {
                	// Choosing the best place to deliver cargo at the end
                    if (me.dropoffs.size() > 0) {
	                    EntityId key2 = me.dropoffs.keySet().iterator().next();
	                    // Shipyard case
	                    if (gameMap.calculateDistance(ship.position, me.shipyard.position) <=
	                    	gameMap.calculateDistance(ship.position, me.dropoffs.get(key2).position) ) {
	                    	
	                		if (me.shipyard.position.y < ship.position.y) {
	                			commandQueue.add(ship.move(Direction.NORTH));
	                		}
	                		else if (me.shipyard.position.y > ship.position.y) {
	                			commandQueue.add(ship.move(Direction.SOUTH));
	                		}
	                		else if (me.shipyard.position.x < ship.position.x) {
	                			commandQueue.add(ship.move(Direction.WEST));
	                		}
	                		else if (me.shipyard.position.x > ship.position.x) {
	                			commandQueue.add(ship.move(Direction.EAST));
	                		}
	                    }
	                    // Dropoff case
	                    else {
	                		if (me.dropoffs.get(key2).position.y < ship.position.y) {
	                			commandQueue.add(ship.move(Direction.NORTH));
	                		}
	                		else if (me.dropoffs.get(key2).position.y > ship.position.y) {
	                			commandQueue.add(ship.move(Direction.SOUTH));
	                		}
	                		else if (me.dropoffs.get(key2).position.x < ship.position.x) {
	                			commandQueue.add(ship.move(Direction.WEST));
	                		}
	                		else if (me.dropoffs.get(key2).position.x > ship.position.x) {
	                			commandQueue.add(ship.move(Direction.EAST));
	                		}
	                    }
                    }
                    else {
                		if (me.shipyard.position.y < ship.position.y) {
                			commandQueue.add(ship.move(Direction.NORTH));
                		}
                		else if (me.shipyard.position.y > ship.position.y) {
                			commandQueue.add(ship.move(Direction.SOUTH));
                		}
                		else if (me.shipyard.position.x < ship.position.x) {
                			commandQueue.add(ship.move(Direction.WEST));
                		}
                		else if (me.shipyard.position.x > ship.position.x) {
                			commandQueue.add(ship.move(Direction.EAST));
                		}
                    }
            	}
            	
            	// 4. Default case
            	else {
            		
            		// If we delivered on shipyard
	            	if (ship.position.equals(me.shipyard.position)) {
	            		way_home.remove(ship.id);
	            	}
	            	
	            	// If we delivered on dropoff
	            	if(me.dropoffs.size() > 0) {
	            		EntityId key1 = me.dropoffs.keySet().iterator().next(); 
	            		if (ship.position.equals(me.dropoffs.get(key1).position)) {
	            			way_home.remove(ship.id);
	            		}
	            	}
	            	
	                // Farm case
	                if (gameMap.at(ship).halite > 50 && ship.halite < 975 && !way_home.contains(ship.id)) {
	                    commandQueue.add(ship.stayStill());
	                    //gameMap.at(ship.position).markUnsafe(ship);
	                }
	                
	                // Delivery case
	                else if (ship.halite > 975 || way_home.contains(ship.id)) {
	                	// If we start delivery now, change status
	                    if (!way_home.contains(ship.id)) {
	                    	way_home.add(ship.id);
	                    }
	                    // Choose delivery place
	                    // If we can deliver on both shipyard/dropoff
	                    if (me.dropoffs.size() > 0) {
		                    EntityId key2 = me.dropoffs.keySet().iterator().next();
		                    // Deliver on shipyard
		                    if (gameMap.calculateDistance(ship.position, me.shipyard.position) <=
		                    	gameMap.calculateDistance(ship.position, me.dropoffs.get(key2).position) ) {
		                    	commandQueue.add(ship.move(gameMap.naiveNavigate(ship, me.shipyard.position)));
		                    }
		                    // Deliver on dropoff
		                    else {
		                    	commandQueue.add(ship.move(gameMap.naiveNavigate(ship, me.dropoffs.get(key2).position)));
		                    }
	                    }
	                    // We can deliver only on shipyard
	                    else {
	                    	commandQueue.add(ship.move(gameMap.naiveNavigate(ship, me.shipyard.position)));
	                    }
	                }
	                // Searching for Halite
	                else {
	                	// around_halite used to determine if a dropoff here should be made
	                	int max = 0, around_halite = 0;
	                	Position nextpos = new Position(0, 0);
	                	// We always search in a diamond-shape
	                	for (int x = -4; x < 4; x++) {
							for (int y = -4 + Math.abs(x); y <= 4 - Math.abs(x); y++) {
								around_halite += gameMap.at(new Position(ship.position.x + x, ship.position.y + y)).halite;
								if (gameMap.at(new Position(ship.position.x + x, ship.position.y + y)).halite > max) {
									if (!gameMap.at(new Position(ship.position.x + x, ship.position.y + y)).position.equals(me.shipyard.position)) {
										max = gameMap.at(new Position(ship.position.x + x, ship.position.y + y)).halite;
										nextpos = new Position(ship.position.x + x, ship.position.y + y);
									}
								}
							}
	                	}
	                	// Enough halite around, a dropoff should be made, instead of moving
	                	if (around_halite >= 8000 && me.halite > 4000 &&
	                	   gameMap.calculateDistance(ship.position, me.shipyard.position) > 10 && 
	                	   me.dropoffs.size() < 1) {	                		
	                		commandQueue.add(ship.makeDropoff());
	                		me.halite -= 4000;
	                	}
	                	// Move to the next position
	                	else {
	                		commandQueue.add(ship.move(gameMap.naiveNavigate(ship, nextpos)));
	                	}	
	                }
            	}

            }

            // Spawning ships until
            if (game.turnNumber <= ((int)(0.41 * Constants.MAX_TURNS)) &&
                me.halite >= ((int)Constants.SHIP_COST + 1000) &&
                (!gameMap.at(me.shipyard).isOccupied()) && 
                me.ships.size() < 15) {
            	
                commandQueue.add(me.shipyard.spawn());
            }

            game.endTurn(commandQueue);
        }
    }
}

