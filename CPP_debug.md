Debugging in C++ folosind CLion si GDB
======================================

### !!! Cititi tot inainte de a va apuca sa testati !!!
1. In fisierul "MyBot.cpp" sau unde este functia main, introducem urmatoarele linii:
    - `#include <zconf.h>`
    - inaintea functiei main: `volatile int ready = 0;`
    - la inceputul functiei main: `while (!ready) sleep(1);`
2. In "CMakeLists.txt" ne asiguram ca linia cu flag-uri de compilare nu contine si flag-ul `-O2`, dar contine flag-ul `-g`. Linia cu flag-uri de compilare ar trebui sa inceapa cu `#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O2 -Wall ...`
3. Copiem executabilul obtinut in urma compilarii engine-ului (probabil se numeste `halite`) in directorul proiectului, daca nu este deja.
4. Facem o noua configuratie de rulare in CLion.
    - `Run -> Edit Configurations...`.
    - Apasam pe `+`, apoi selectam `Application`
    - Punem ce nume vrem (ex: `debug`), la target lasam cum e (probabil MyBot), la executable selectam `halite` (executabilul engine-ului).
    - La `Program arguments` puteti pune ce ati pune daca ati executa `./halite` din terminal (dimensiunea hartii, seed, botii rulati etc.). Neaparat sa puneti flag-ul `--no-timeout`, iar botul vostru va avea calea `cmake-build-debug/MyBot` (sau in loc de MyBot ce nume are executabilul vostru in urma compilarii).
5. O data facuta configuratia, rulati si ar trebui sa apara in terminalul din CLion ca se porneste meciul, dar se blocheaza la initializarea jucatorilor. Acum dati `Ctrl+Shift+X` sau `Run -> Attach to Local Process...` si cautati `MyBot` (sau numele executabilului botului vostru).
6. (Optional) Daca da fail atasarea la proces, rulati comanda urmatoare intr-un terminal `echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope`. Vedeti [aici](https://www.jetbrains.com/help/clion/attaching-to-local-process.html) ce trebuie facut daca nu vreti sa rulati comanda dupa fiecare restart al sistemului.
7. Acum ar trebui sa apara in partea de jos un panou de debug cu 4 tab-uri (`Frames`, `Variables`, `Console` si `GDB`), iar in stanga acestui panou ar trebui sa vedeti niste iconite ca la un player video, dati pe cea de pauza (linii verticale) si apoi in tab-ul GDB executati comanda `set ready = 1`
8. Acum inainte sa dati din nou play din debugger, puneti un breakpoint unde vreti in cod, si daca se ajunge acolo, programul ar trebui sa se opreasca in punctul acela.

### NU UITATI SA ELIMINATI LINIILE DE LA PUNCTUL 1 INAINTE DE A TRIMITE TEMA SAU CAND DATI PUSH VARIANTEI FINALE PENTRU O ETAPA PE GITLAB

Recomandari:
- Rulati un singur bot in modul debug (cu liniile mentionate la punctul 1) pentru ca altfel trebuie sa dati attach la fiecare proces ca sa setati `ready = 1`
- Puteti seta conditii pentru breakpoint, astfel ca programul sa se opreasca doar cand anumite variabile din cod au valorile potrivite (de exemplu sa se opreasca la un breakpoint doar dupa tura 200 a jocului)
- Daca va puteti atasa la proces, dar breakpoint-uri ce sigur ar trebui sa fie atinse, nu sunt atinse, sau programul pare ca se opreste la alta linie, probabil ati uitat sa scoateti `-O2` sau sa adaugati `-g` la flag-urile de compilare.

Daca intampinati probleme cu acest tutorial, trimiteti un mail la: george.diaconu2208@stud.acs.upb.ro  
Fiti cat mai expliciti in mail, de preferat ar fi sa includeti un print screen cu configuratia si cu liniile de cod puse in main.