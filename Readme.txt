Instructiuni de compilare:
python ./run.py --cmd "java MyBot" --round 2


Detalii despre structura proiectului:
 - Am ales sa rescriem intreaga strategie
 - Am realizat intreaga implementare in main-ul din MyBot


Detalii despre abordarea algoritmica a etapei:
 - Am creat un ArrayList in care stocam fiecare nava ce trebuie sa livreze
 cargo-ul acasa, daca are peste 975 halite, este adaugata in lista doar in
 cazul in care nu este deja. In momentul in care a livrat cargo-ul (este pe
 shipyard/dropoff), este scoasa din lista.
  - Primul ship este intotdeauna un "Joker" (se duce direct pe shipyard-ul 
 inamicului) si nu il lasa sa livreze cargo-ul.
  - Am adaugat si o conditie pentru o situatie random - in cazul in care
 vreun ship ajunge pe shipyard-ul inamicului, va ramane acolo blocandu-l.
  - Miscarea este divizata in 2 posibile scenarii - primele MaxTurns - 20
 ture si ultimele 20 de ture.
  - In ultimele 20 de ture toate ship-urile vor da crash pe shipyard/dropoff
 in functie de cat de apropiate sunt de fiecare.
  - Fiecare nava scaneaza zona in forma de romb din jurul ei. Am stabilit ca
 asta e modul optim de cautare, din moment ce mersul pe orizontala/verticala
 ne va costa doar o 1 tura (si mai putin halite pe cazul general) in timp ce
 mersul pe diagonala ne va costa 2 ture (dublu halite pierdut).
  - Dupa ce ajunge la MapCell-ul cu cel mai mult halite, farmeaza pana cand
 halite-ul de pe cel > 50 si cargo-ul este mai mic de 975.
  - In momentul in care trebuie sa livreze cargo-ul se face un minim intre
 distanta pana la shipyard si distanta pana la dropoff (daca exista), si 
 astfel se alege punctul de livrare.
  - In scanarea de mai sus, se tine cont in variabila "around_halite" si de
 cat halite este in acel romb, prima zona cu mai mult de 8000 halite, la 
 distanta mai mare de 10 celulte de shipyard devenind astfel locul pentru 
 dropoff-ul nostru.


 Surse de inspiratie:
  - Post-Mortem-ul lui Chessjawa (#25)
